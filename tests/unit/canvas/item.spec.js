import { shallowMount } from "@vue/test-utils";
import Item from "@/components/canvas/Item.vue";

let wrapper;
const title = "competencies 1";
const icon = "icon 1";
const field = "product";
const value = "something";

describe("Item.vue", () => {
    beforeEach(() => {
        wrapper = shallowMount(Item, {
            slots: {
                title,
                icon,
            },
            propsData: {
                field,
                value,
            },
        });
    });

    it("renders slots and props", async () => {
        expect(wrapper.find("h2").text()).toBe(icon + " " + title);
        expect(wrapper.props().field).toBe(field);
        expect(wrapper.props().value).toBe(value);
    });

    it("can trigger input event in textarea", async () => {
        await wrapper.find("form textarea").trigger("input");
        expect(wrapper.vm.written).toBe(true);
        expect(wrapper.vm.saving).toBe(true);
        expect(wrapper.find("p").text()).toContain("Guardando ...");
    });
});
