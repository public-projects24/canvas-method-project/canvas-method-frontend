# Tablero Canvas (Frontend)

En este proyecto podras iniciar sesión con usuario y contraseña para luego ser dirigido al tablero canvas que podras editar.

## Especificaciones

Este proyecto esta hecho en Vue 2 y usa axios, vuex, vue-router, sass y cookie-js. Necesitas tener instalado nodejs y npm.

## Instalación

1. Clona este repositorio en tu local
2. Ingresa a la carpeta del proyecto e instala los paquetes con npm, ejectuando el siguiente comando en la terminal.

```
npm install
```

3. Duplica el archivo .env.example que está en la raiz y renombralo por .env
4. Edita el archivo .env y modifica la variable "VUE_APP_SITE_API" cambiando el valor por el host en el que se encuentre configurado el proyecto backend (canvas-method-backend).

5. Ejecuta los test unitarios

```
npm run test:unit
```

5. Compila el proyecto en modo desarrollo ejectuando el siguiente comando en la terminal.

```
npm run serve
```

6. Entra a la dirección que aparece en la terminal
7. Listo, puedes probarla.
