import Auth from "@/services/Auth";
import Cookie from "js-cookie";

export default {
    namespaced: true,
    state: () => ({
        token: Cookie.get("token") ? Cookie.get("token") : null,
        user: null,
    }),
    getters: {
        isAuthenticated(state) {
            return state.token != null ? true : false;
        },
        getToken(state) {
            return state.token;
        },
        getAuthorizationHeaders(state) {
            return {
                headers: {
                    Authorization: "Bearer " + state.token,
                },
            };
        },
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },
    },
    actions: {
        async login(context, form) {
            let { data } = await Auth.login(form);
            context.commit("setToken", data.token);
            Cookie.set("token", data.token, { expires: 1 });
        },
        async logout(context) {
            //let { data } = await Auth.logout();
            //console.log(data);
            context.commit("setToken", null);
            Cookie.remove("token");
        },
    },
};
