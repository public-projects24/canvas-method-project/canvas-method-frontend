import Api from "@/services/Api";
import store from "@/store";

export default {
    get() {
        return Api.get(
            "/api/canvas",
            store.getters["auth/getAuthorizationHeaders"]
        );
    },
    update(data) {
        data["_method"] = "put";
        return Api.post(
            "/api/canvas",
            data,
            store.getters["auth/getAuthorizationHeaders"]
        );
    },
};
