import store from "@/store";

export default (to, from, next) => {
    //console.log("guest middleware");

    if (store.getters["auth/isAuthenticated"]) {
        next({ name: "dashboard" });
    } else {
        next();
    }
};
