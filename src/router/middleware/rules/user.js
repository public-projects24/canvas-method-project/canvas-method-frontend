import store from "@/store";

export default (to, from, next) => {
    //console.log("user middleware");

    if (store.getters["auth/isAuthenticated"]) {
        next();
    } else {
        next({ name: "login" });
    }
};
