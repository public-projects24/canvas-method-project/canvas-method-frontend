import Vue from "vue";
import VueRouter from "vue-router";
import middleware from "@/router/middleware";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "login",
        component: () =>
            import(/* webpackChunkName: "login" */ "../views/Login.vue"),
        beforeEnter: middleware.guest,
    },
    {
        path: "/dashboard",
        name: "dashboard",
        component: () =>
            import(
                /* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"
            ),
        beforeEnter: middleware.user,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
